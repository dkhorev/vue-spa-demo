# vue-spa-demo

This is frontend part of a MVP for a bigger application.

The goal of application is to integrate with Bitrix24 CRM for keeping track of company's financial health.

Real project has a backend part, is based on Laravel components. Has multi-tenant client databases, migrations etc.

Frontend can work in demo mode, when all the data is local and in production mode.

Built project is in /public folder.

