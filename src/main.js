// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/router'
// import VeeValidate from 'vee-validate'
import axios from 'axios'
import store from './store/store'
import jQuery from 'jquery'
import 'popper.js'
import 'bootstrap'

window.axios = axios

// Vue.use(VeeValidate)
Vue.config.productionTip = false

/* Enable Bootstrap tooltips */
// global declaration of jquery
global.jQuery = jQuery
global.$ = jQuery
$(() => {
  $('#app').tooltip({
    selector: '[data-toggle="tooltip"]'
  })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {},
  router,
  store,
  render: h => h(App)
})
