export default {
  Const: {
    LABEL_BALANCE: 'Баланс',
    LABEL_REVENUE: 'Доходы',
    LABEL_COST: 'Расходы',
    LABEL_EXPECTED_REVENUE: 'Ожид. доход',
    LABEL_EXPECTED_COST: 'Ожид. расход',
    COLOR_BALANCE: 'rgba(0, 102, 0, 1)',
    COLOR_REVENUE: 'rgba(0, 0, 255, 1)',
    COLOR_COST: 'rgba(255, 0, 0, 1)',
    COLOR_EXPECTED_REVENUE: 'rgba(204, 204, 51, 0.8)',
    COLOR_EXPECTED_COST: 'rgba(204, 51, 51, 0.8)'
  }
}
