import LineChart from './LineChart'
import Api from '../../store/api'
import helpers from '../../mixins/helpers'
import { mapGetters } from 'vuex'

export default {
  mixins: [helpers],
  data () {
    return {}
  },
  components: {LineChart},
  computed: {
    ...mapGetters({
      queryMonth: 'queryMonth',
      queryYear: 'queryYear',
      arOps: 'getOperations',
      isDemo: 'isDemo'
    })
  },
  methods: {
    update: function () {
      // демо графики
      if (this.isDemo) {
        this.dataCollection.labels = JSON.parse(JSON.stringify(LineChart.data().demoChart.labels))
        this.dataCollection.datasets = JSON.parse(JSON.stringify(LineChart.data().demoChart.datasets))
        // trigger real update
        this.$refs.myChart.update()
      } else {
        // запрос к апи
        const reqData = this.prepareRequest()
        console.log('Chart update', reqData)
        this.updateChart(Api.getChartData(reqData))
      }
    },
    async updateChart (promise) {
      const that = this

      await promise.then(response => {
        const result = response.data.main
        if (result) {
          // update labels from response
          that.dataCollection.labels = result.labels
          // clear datasets
          that.dataCollection.datasets = []
          // update datasets from response
          result.datasets.forEach((set, index) => {
            const setOptions = this.datasetTemplate[index]
            setOptions.data = set
            this.dataCollection.datasets.push(setOptions)
          }, that)
          // trigger real update
          that.$refs.myChart.update()
        }
      })
    }
  },
  mounted () {
    this.update()
  },
  watch: {
    arOps: function () {
      this.update()
    }
  }
}
