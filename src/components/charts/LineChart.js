import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  props: ['data', 'options'],
  data () {
    return {
      demoChart: {
        labels: ['1', '2', '3', '4', '5', '6'],
        datasets: [
          {
            label: 'Баланс',
            backgroundColor: 'rgba(0, 102, 0, 1)',
            fill: false,
            borderColor: 'rgba(0, 102, 0, 1)',
            pointBackgroundColor: 'rgba(0, 102, 0, 1)',
            pointBorderColor: 'rgba(0, 102, 0, 1)',
            pointRadius: 10,
            pointHoverRadius: 12,
            data: [100, 300, 200, 500, 400, 600]
          }
        ]
      }
    }
  },
  mounted () {
    console.log('LineChart mounted')
    this.renderChart(this.data, this.options)
  },
  methods: {
    update () {
      console.log('LineChart update', this.data)
      this.$data._chart.update()
    }
  }
}
