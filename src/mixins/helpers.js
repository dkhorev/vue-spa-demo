export default {
  data () {
    return {
      formatter: new Intl.NumberFormat('ru-RU', {style: 'currency', currency: 'RUB', maximumFractionDigits: 0, minimumFractionDigits: 0})
    }
  },
  methods: {
    // форматер валюты
    formatCurrency (value) {
      // console.log('formatCurrency', value)
      value = parseFloat(value)
      return this.formatter.format(value)
    }
  }
}
