import { mapActions, mapGetters } from 'vuex'
// import { mapGetters } from 'vuex'
import AddBtn from '../components/forms/AddBtn'
import EditBtn from '../components/forms/EditBtn'
import DeleteBtn from '../components/forms/DeleteBtn'
import DeleteForm from '../components/forms/DeleteForm'

export default {
  data () {
    return {
      arNew: {},
      arEdit: {}
    }
  },
  components: {AddBtn, EditBtn, DeleteBtn, DeleteForm},
  computed: {
    ...mapGetters({
      isAppReady: 'isAppReady',
      arContractors: 'getContractors'
    }),
    arFirms: function () {
      return this.arContractors.filter(it => it.is_my === 1)
    },
    arCompanies: function () {
      return this.arContractors.filter(it => it.is_my === 0)
    }
  },
  methods: {
    ...mapActions(['resizeB24Frame'])
  },
  mounted () {
    this.resizeB24Frame()
  }
}
