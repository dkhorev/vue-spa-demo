export default {
  // активные сотрудники имеют флаг + департаменты
  activeEmployee (data) {
    return data.filter(empl => (empl.is_active === 1 && empl.departments))
  },
  // фильтрует к показу Операции текущего месяца (глобальный, есть всегда, только демо)
  byCurrentMonth: function (state, data) {
    const dateFrom = new Date(state.queryYear, state.queryMonth, 1, 0, 0, 0, 0)
    const dateTo = new Date(state.queryYear, state.queryMonth + 1, 1, 0, 0, 0, 0)

    return data.filter(item => {
      const dateItem = new Date(item.date)
      // console.log('dateItem', dateItem)
      // console.log('dateFrom', dateFrom)
      // console.log('dateItem >= dateFrom', dateItem.getTime() >= dateFrom.getTime())
      //
      // console.log('dateItem', dateItem)
      // console.log('dateTo', dateTo)
      // console.log('dateItem < dateTo', dateItem.getTime() < dateTo.getTime())
      // console.log(' ')

      return dateItem.getTime() >= dateFrom.getTime() && dateItem.getTime() < dateTo.getTime()
    })
  },
  // по значениею в поле
  byFieldValue: function (data, field, value, state) {
    let arValues = []
    let filterField = ['name']
    switch (field) {
      case 'paytype_id':
        arValues = state.Paytype
        break
      case 'firm_id':
        arValues = state.Contractor
        break
      case 'contractor_id':
        arValues = state.Contractor
        break
      case 'employee_id':
        arValues = state.Employee
        filterField = ['name', 'lastname']
        break
      case 'manager_id':
        arValues = state.Employee
        filterField = ['name', 'lastname']
        break
    }

    console.log('Filters.byFieldValue filtering', field, value, arValues)
    if (arValues.length > 0) {
      let arGoodValues = arValues.filter(item => {
        // console.log('filterField', filterField)
        let itemValue = []
        filterField.forEach(val => {
          itemValue.push(item[val].toLowerCase())
        })
        itemValue = itemValue.join(' ')
        let searchValue = value.toLowerCase()
        // console.log(itemValue, searchValue)
        return itemValue.search(searchValue) > -1
      })
      arGoodValues = arGoodValues.map(item => item.id)
      // console.log(arGoodValues)
      return data.filter(item => arGoodValues.indexOf(item[field]) > -1)
    }

    return data
  },
  // по текстовому значению
  byValue: function (data, field, value) {
    return data.filter(item => {
      let itemValue = item[field].toString().toLowerCase()
      let searchValue = value.toString().toLowerCase()
      return itemValue.search(searchValue) > -1
    })
  },
  // по числовому значению
  byNumber: function (data, field, value) {
    return data.filter(item => parseFloat(item[field]) === parseFloat(value))
  }
}
