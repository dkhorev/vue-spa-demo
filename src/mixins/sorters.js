export default {
  // простая сортировка по полю
  byField: function (data, field, order) {
    console.log('Sorters.byField sorting', field, order)
    return data.sort((a, b) => {
      if (order === 'asc') {
        if (a[field] > b[field]) {
          return 1
        }
        if (a[field] < b[field]) {
          return -1
        }
        // a должно быть равным b
        return 0
      } else {
        if (b[field] > a[field]) {
          return 1
        }
        if (b[field] < a[field]) {
          return -1
        }
        // a должно быть равным b
        return 0
      }
    })
  },
  // сортировка по дате
  byDate: function (data, field, order) {
    return data.sort((a, b) => {
      const dateA = new Date(a[field]).getTime()
      const dateB = new Date(b[field]).getTime()

      if (order === 'desc') {
        return dateB - dateA
      } else {
        return dateA - dateB
      }
    })
  },
  // сортировка чисел
  byNumber: function (data, field, order) {
    return data.sort((a, b) => {
      if (order === 'desc') {
        return b[field] - a[field]
      } else {
        return a[field] - b[field]
      }
    })
  },
  // по значениею в поле
  byFieldValue: function (data, field, order, state) {
    let arValues = []
    let sortField = 'name'
    switch (field) {
      case 'paytype_id':
        arValues = state.Paytype
        break
      case 'firm_id':
        arValues = state.Firm
        break
      case 'contractor_id':
        arValues = state.Contractor
        break
      case 'employee_id':
        arValues = state.Employee
        break
      case 'manager_id':
        arValues = state.Employee
        break
    }

    console.log('Sorters.byFieldValue sorting', field, order, arValues)
    if (arValues.length > 0) {
      return data.sort((a, b) => {
        let valA = arValues.filter(item => item.id === a[field])[0]
        if (valA) {
          valA = valA[sortField]
        } else {
          valA = ''
        }

        let valB = arValues.filter(item => item.id === b[field])[0]
        if (valB) {
          valB = valB[sortField]
        } else {
          valB = ''
        }
        // console.log('IDs ', valA, valB)
        if (order === 'asc') {
          if (valA > valB) {
            return 1
          }
          if (valA < valB) {
            return -1
          }
          // a должно быть равным b
          return 0
        } else {
          if (valB > valA) {
            return 1
          }
          if (valB < valA) {
            return -1
          }
          // a должно быть равным b
          return 0
        }
      })
    }

    return data
  }
}
