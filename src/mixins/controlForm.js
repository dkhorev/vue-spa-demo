import { mapActions, mapGetters } from 'vuex'

export default {
  data () {
    return {
      // дефолтные поля
      arData: {},
      formStatus: false,
      formStatusEdit: false
    }
  },
  computed: {
    ...mapGetters({
      arPaytypes: 'getPaytypes',
      arTypeVariants: 'getPaytypeVariants',
      arEmployees: 'getEmployees',
      arContractors: 'getContractors',
      arErrors: 'arErrors',
      arForms: 'arForms',
      arEditForms: 'arEditForms',
      operation: 'formOperation',
      isDemo: 'isDemo',
      modalOffset: 'modalOffset'
    }),
    arFirms: function () {
      return this.arContractors.filter(it => it.is_my === 1)
    },
    arCompanies: function () {
      return this.arContractors.filter(it => it.is_my === 0)
    },
    hasErrors: function () {
      // console.log('hasErrors', this.model, this.arErrors[this.model])
      if (this.arErrors[this.model] === undefined) {
        return false
      }
      return this.arErrors[this.model].length > 0
    },
    errorText: function () {
      if (this.model === undefined) {
        return ''
      }
      return this.arErrors[this.model]
    }
  },
  methods: {
    ...mapActions(['actionAdd', 'setFormState', 'actionUpdate', 'setEditState', 'setFormSpinner']),
    // отправка формы
    submitForm () {
      const arNew = JSON.parse(JSON.stringify(this.arData))

      this.setFormSpinner({status: true})
      switch (this.operation) {
        case 'add':
          this.actionAdd({
            model: this.model,
            data: arNew
          })
          break
        case 'edit':
          this.actionUpdate({
            model: this.model,
            data: arNew
          })
          break
      }
    },
    // сброс полей
    resetData () {
      console.log('AddElementFrom resetData')
      this.arData = JSON.parse(JSON.stringify(this.arDefault))
      // в формах автокомплита надо сбросить значения
      this.$children.forEach(vm => {
        if (vm.$options.name === 'Autocomplete') {
          vm.setDefaultText(this.arData[vm.propName])
        }
      })
    },
    // следим за статусом формы чтобы закрыть когда успех обработки добавления
    formWatch () {
      // console.log('formWatch')
      // console.log(this.operation)
      switch (this.operation) {
        case 'add':
          this.formStatus = this.arForms[this.model]
          break
        case 'edit':
          this.formStatusEdit = this.arEditForms[this.model]
          break
      }
      return ''
    },
    // закрытие формы
    closeForm () {
      switch (this.operation) {
        case 'add':
          this.setFormState({model: this.model, status: false})
          break
        case 'edit':
          this.setEditState({model: this.model, index: false})
          break
      }
    }
  },
  created () {
    switch (this.operation) {
      case 'add':
        // console.log('AddElementFrom created')
        this.resetData()
        this.formStatus = this.arForms[this.model]
        break
      case 'edit':
        this.resetData()
        break
    }
  },
  watch: {
    // следим за формой и закрываем/сбрасыаем при удачном добавлении элемента
    formStatus: function (oldVal, newVal) {
      // console.log(oldVal, newVal)
      if (this.formStatus === false) {
        console.log('AddElementFrom closing')
        $('.dismiss-btn').click()
      } else {
        console.log('AddElementFrom opening')
      }
      this.resetData()
    },
    formStatusEdit: function (oldVal, newVal) {
      // console.log(oldVal, newVal)
      // console.log(this.formStatusEdit)
      if (this.formStatusEdit === false) {
        // console.log('EditElementFrom closing')
        $('.dismiss-btn').click()
      } else if (this.formStatusEdit >= 0) {
        console.log('EditElementFrom opening')
        // устанавливаемз начения в форму
        this.arData = JSON.parse(JSON.stringify(this.arModelData[this.formStatusEdit]))

        // в формах автокомплита надо обновить значения по умолчанию
        this.$children.forEach(vm => {
          if (vm.$options.name === 'Autocomplete') {
            vm.setDefaultText(this.arData[vm.propName])
          }
        })
      }
    }
  }
}
