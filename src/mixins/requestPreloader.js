import { mapGetters } from 'vuex'

export default {
  data () {
    return {}
  },
  computed: {
    ...mapGetters(['apiRequestInProgress']),
    whenRequestInProgress: function () {
      // console.log('whenRequestInProgress', this.apiRequestInProgress, this.model)
      this.verifyStatus()
      return this.apiRequestInProgress
    }
  },
  methods: {
    // закрывает модалку при правильных условиях
    // форма должна реализовывать computed метод hasErrors
    verifyStatus () {
      // console.log('verifyStatus', this.apiRequestInProgress, this.hasErrors, this.model)
      // console.log(this)
      if (!this.hasErrors && !this.apiRequestInProgress) {
        // небольшой хак, т.к. кнопки закрытия немного конфликтовали
        if (undefined === this.model) {
          // console.log('clicking dismiss-btn DeleteForm')
          $('.dismiss-btn-delete').click()
        } else {
          // console.log('clicking dismiss-btn ModelForm')
          $('.dismiss-btn').click()
        }
      }
    }
  }
}
