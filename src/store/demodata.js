export default {
  Employees: [
    {
      'id': 1,
      'name': 'Вася',
      'lastname': 'Пупкин',
      'is_remote': 1,
      'roi': 0,
      'card_number': '2244 5678 3345 2233 5555',
      'wage_real': 10000,
      'wage': 1000,
      'b24_id': null,
      'is_active': 0,
      'departments': '1,2'
    },
    {
      'id': 2,
      'name': 'Петя',
      'lastname': 'Матросов',
      'is_remote': 0,
      'roi': 0,
      'card_number': '9988 1111 2222 2233 5555',
      'wage_real': 12000,
      'wage': 12000,
      'b24_id': null,
      'is_active': 1,
      'departments': '3'
    },
    {
      'id': 3,
      'name': 'Коля',
      'lastname': '',
      'is_remote': 1,
      'roi': 0,
      'card_number': '2244 5678 3345 2233 5555',
      'wage_real': 25000,
      'wage': 500,
      'b24_id': null,
      'is_active': 0,
      'departments': '4'
    },
    {
      'id': 4,
      'name': 'Иван',
      'lastname': '',
      'is_remote': 1,
      'roi': 0,
      'card_number': '2244 5678 3345 2233 5555',
      'wage_real': 55000,
      'wage': 27500,
      'b24_id': null,
      'is_active': 1,
      'departments': '5'
    },
    {
      'id': 5,
      'name': 'Алексей',
      'lastname': 'Петров',
      'is_remote': 0,
      'roi': 0,
      'card_number': '2244 5678 3345 2233 5555',
      'wage_real': 155000,
      'wage': 5000,
      'b24_id': null,
      'is_active': 0,
      'departments': '6'
    }
  ],

  Costs: [
    {
      'id': 11,
      'name': 'Аренда',
      'price': 5000
    },
    {
      'id': 22,
      'name': 'Офис',
      'price': 15000
    },
    {
      'id': 33,
      'name': 'Бухгалтерия',
      'price': 20000
    }
  ],

  Paytypes: [
    {'id': 1, 'name': 'Лид в работе', 'type': 1},
    {'id': 2, 'name': 'Лид проигран', 'type': 7},
    {'id': 3, 'name': 'Перенос', 'type': 2},
    {'id': 4, 'name': 'Приход', 'type': 3},
    {'id': 5, 'name': 'Расход', 'type': 4},
    {'id': 6, 'name': 'Ожидаемый приход', 'type': 5},
    {'id': 7, 'name': 'Ожидаемый расход', 'type': 6},
    {'id': 8, 'name': 'Фиксированные расходы', 'type': 4}
  ],

  PaytypeVariants: [
    {'id': 1, 'name': 'Лид в работе', 'code': 'TYPE_LEAD'},
    {'id': 2, 'name': 'Перенос', 'code': 'TYPE_OVER'},
    {'id': 3, 'name': 'Приход', 'code': 'TYPE_REVENUE'},
    {'id': 4, 'name': 'Расход', 'code': 'TYPE_COST'},
    {'id': 5, 'name': 'Ожидаемый приход', 'code': 'TYPE_EXPECT_REVENUE'},
    {'id': 6, 'name': 'Ожидаемый расход', 'code': 'TYPE_EXPECT_COST'},
    {'id': 7, 'name': 'Лид проигран', 'code': 'TYPE_LEAD_LOST'}
  ],

  Contractors: [
    {'id': 1, 'name': 'ООО Рога и Копыта', 'b24_id': 78, 'is_company': 1, 'is_my': 0},
    {'id': 2, 'name': 'ПАО Морковка', 'b24_id': 45, 'is_company': 1, 'is_my': 0},
    {'id': 3, 'name': 'ЗАО Клубничка', 'b24_id': '', 'is_company': 1, 'is_my': 0},
    {'id': 4, 'name': 'ИП www-ru', 'b24_id': 25, 'is_company': 0, 'is_my': 0},
    {'id': 5, 'name': 'ИП', 'b24_id': 99, 'is_company': 1, 'is_my': 1},
    {'id': 6, 'name': 'Нал', 'b24_id': '', 'is_company': 1, 'is_my': 1},
    {'id': 7, 'name': 'Банк', 'b24_id': '', 'is_company': 1, 'is_my': 1},
    {'id': 8, 'name': 'Альфа', 'b24_id': 25, 'is_company': 1, 'is_my': 1}
  ],

  // нужна хотя бы одна операция на входе, чтобы vue запомнил поля
  Operations: [
    {
      'id': 1,
      'paytype_id': 8,
      'firm_id': '',
      'contractor_id': '',
      'manager_id': null,
      'employee_id': '',
      'summ': 40000,
      'date': (new Date()).toISOString().slice(0, 8) + '01', // первый день
      'account': '',
      'comment': 'Аренда, офис, бухгалетрия'}
  ],

  Departments: [
    {'id': 1, 'name': 'Руководители'},
    {'id': 2, 'name': 'Бухгалтерия'},
    {'id': 3, 'name': 'Маркетинг'},
    {'id': 4, 'name': 'Разработка веб-проектов'},
    {'id': 5, 'name': 'Отдел автоматизации п/п'},
    {'id': 6, 'name': 'Менеджеры проектов'}
  ],

  MonthlyTask: {
    open_bills: 1,
    open_leads: 1,
    wage_50: 1,
    wage_100: 1,
    fixed_costs: 1
  },

  Options: [
    {id: 1, key: 'demo', value: 'demo'}
  ]
}
