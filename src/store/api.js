import axios from 'axios/index'
import loMerge from 'lodash/merge'

// import qs from 'qs'
// пример post, но он на nginx не будет работать сразу, безотказно все-таки через get...
// let zz = {id: 1, val: 55}
// axios.post(this.apiUrl, qs.stringify(zz)
// ).then(function (response) {
//   console.log(response)
// }).catch(function (error) {
//   console.log(error)
// })

export default {
  // данные из моделей на бэкенде
  Const: {
    CHART_WEEK: 1, // Statistic
    CHART_MONTH: 2,
    CHART_DAY: 3,
    PAYTYPE_REVENUE: 3, // Paytype
    PAYTYPE_COST: 4,
    PAYTYPE_EXPECT_REVENUE: 5,
    PAYTYPE_EXPECT_COST: 6,
    CODE_LENGTH: 32 // длина ключа стандартная
  },
  // apiUrl: 'http://profit.brainy-crm.loc/api/v1/', // адрес API
  apiUrl: 'https://profit.web-now.run/api/v1/', // адрес API
  apiCode: {'code': false}, // ключ апи
  setAuth (sCode) {
    this.apiCode.code = sCode
  },
  // авторизация Битрикс24
  getAuth () {
    return this.apiCode
  },
  // запрос данных GetList => все данные разово
  getListRequest (model, context, moreParams = {}) {
    // console.log('getListRequest', moreParams)
    axios.get(this.apiUrl, {
      params: loMerge({operation: 'get', model}, this.getAuth(), moreParams)
    }).then(function (response) {
      context.commit('setDataFromApi', {model, data: response.data.main})

      // только при загрузке моделей есть смысл применять фильтрацию
      if (model === 'Operation') {
        context.dispatch('applyFiltersAndSort')
          .then(context.commit('setAppReadyState', {status: true}))
      }
    }).catch(function (error) {
      console.log(error)
    })
  },

  // запросы add новой сущности
  addRequest (context, request) {
    axios.get(this.apiUrl, {
      params: loMerge({operation: 'add', model: request.model, data: request.data}, this.getAuth())
    }).then(function (response) {
      if (response.data.main.error === true) {
        const errtext = response.data.main.msgHuman ? response.data.main.msgHuman : response.data.main.msg
        context.commit('setErrorState', {model: request.model, 'msg': errtext})
      } else {
        context.commit('setErrorState', {model: request.model, 'msg': ''})
        context.commit('setFormState', {model: request.model, status: false})

        response.data.additional.forEach(item => context.commit('addDataFromApi', {model: item.model, data: item.add}))
        context.commit('addDataFromApi', {model: request.model, data: response.data.main})

        // только при загрузке моделей есть смысл применять фильтрацию
        if (request.model === 'Operation') {
          context.dispatch('applyFiltersAndSort')
        }
      }
      context.commit('setFormSpinner', {status: false})
    }).catch(function (error) {
      console.log(error)
    })
  },

  // запросы delete сущности
  deleteRequest (context, request) {
    axios.get(this.apiUrl, {
      params: loMerge({operation: 'delete', model: request.model, id: request.id}, this.getAuth())
    }).then(function (response) {
      if (response.data.main.error === true) {
        const errtext = response.data.main.msgHuman ? response.data.main.msgHuman : response.data.main.msg
        context.commit('setErrorState', {model: request.model, 'msg': errtext})
      } else {
        context.commit('setErrorState', {model: request.model, 'msg': ''})
        context.commit('deleteDataFromApi', request)
        context.commit('setDeleteTarget', {})

        // только при загрузке моделей есть смысл применять фильтрацию
        if (request.model === 'Operation') {
          context.dispatch('applyFiltersAndSort')
        }
      }
      context.commit('setFormSpinner', {status: false})
    }).catch(function (error) {
      console.log(error)
    })
  },

  // запросы update сущности
  updateRequest (context, request) {
    axios.get(this.apiUrl, {
      params: loMerge({operation: 'update', model: request.model, data: request.data}, this.getAuth())
    }).then(function (response) {
      if (response.data.main.error === true) {
        const errtext = response.data.main.msgHuman ? response.data.main.msgHuman : response.data.main.msg
        context.commit('setErrorState', {model: request.model, 'msg': errtext})
      } else {
        context.commit('setErrorState', {model: request.model, 'msg': ''})
        context.commit('setEditState', {model: request.model, index: false})

        response.data.additional.forEach(item => context.commit('addDataFromApi', {model: item.model, data: item.add}))
        context.commit('updateDataFromApi', {model: request.model, data: response.data.main})

        // только при загрузке моделей есть смысл применять фильтрацию
        if (request.model === 'Operation') {
          context.dispatch('applyFiltersAndSort')
        }
      }
      context.commit('setFormSpinner', {status: false})
    }).catch(function (error) {
      console.log(error)
    })
  },

  // запросы на выполнение Ежемесячных заданий (model === MonthlyTask)
  taskRequest (context, request) {
    axios.get(this.apiUrl, {
      params: loMerge(request, this.getAuth())
    }).then(function (response) {
      if (response.data.main.error === true) {
        const errtext = response.data.main.msgHuman ? response.data.main.msgHuman : response.data.main.msg
        context.commit('setErrorState', {model: request.model, 'msg': errtext})
      } else {
        context.commit('setErrorState', {model: request.model, 'msg': ''})
        context.dispatch('getOperationData')
      }
    }).catch(function (error) {
      console.log(error)
    })
  },

  // закачка файла
  sendFile (formData) {
    console.log('sendFile api call')
    formData.append('operation', 'uploadFile')
    formData.append('code', this.getAuth().code)
    return axios.post(this.apiUrl,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    )
  },

  // запрос графика
  getChartData (request) {
    return axios.get(this.apiUrl, {
      params: loMerge(request, this.getAuth())
    })
  },

  // логин по ключу
  loginByKey (request) {
    return axios.get(this.apiUrl, {
      params: request
    })
  },

  // ручное обновление статусов
  updateStatuses (request) {
    return axios.get(this.apiUrl, {
      params: loMerge(request, this.getAuth())
    })
  },

  // комманда обработать папку с файлами Я.диск
  parseYadiskBills (request) {
    return axios.get(this.apiUrl, {
      params: loMerge(request, this.getAuth())
    })
  }
}
