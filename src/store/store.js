/* global BX24 */
import Vue from 'vue'
import Vuex from 'vuex'
import DemoData from './demodata'
import Api from './api'
import Filters from '../mixins/filters'
import Sorters from '../mixins/sorters'
const loSampe = require('lodash/sample')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // !! статус подгрузки скрипта BX24 !!
    isBx24Ready: false, // будет true если модуль BX24 удачно подключился
    isDemo: false, // будет true если нет подключения к BX24
    nBx24ReadyCount: 0, // счетчик попыток инита BX24
    nBx24ReadyLimit: 5, // лимит попыток -> включаем режим демо
    isAppReady: false, // флаг когда можно начинать рендерить таблицы с данными
    urlPortal: '', // урл портала клиента при локальном запуке приложения (он пишет сам)
    // ** Данные приложения
    Employee: DemoData.Employees, // список Сотрудников
    Cost: DemoData.Costs, // список Расходы
    Paytype: DemoData.Paytypes, // список Типы платежей (данные)
    PaytypeVariant: DemoData.PaytypeVariants, // сами типы платежей (фикс дата)
    Contractor: DemoData.Contractors, // список всех компаний с бэкенда (свои, чужие и т.д.)
    Operation: [], // Транзакции с бэкенда
    OperationFiltered: [], // Транзакции видимые
    OperationDemo: DemoData.Operations, // Транзакции демо режима, не фильтрованные
    Option: DemoData.Options, // Кастомные настройки портала
    Departments: DemoData.Departments, // отделы Компании
    queryMonth: (new Date()).getMonth(), // месяц для выборки Транзакции
    queryYear: (new Date()).getFullYear(), // год для выборки Транзакции
    modalOffset: 0, // оффсет модалок бутстрапа (редактирование/удаление)
    // Error state (состояние ошибок всех форм)
    arErrors: {
      'Employee': '',
      'Cost': '',
      'Paytype': '',
      'Contractor': '',
      'Operation': '',
      'MonthlyTask': ''
    },
    // Form state (состояние видимости форм добавления)
    arForms: {
      'Employee': false,
      'Cost': false,
      'Paytype': false,
      'Contractor': false,
      'Operation': false
    },
    // Form state (состояние видимости форм редактирования)
    arEditForms: {
      'Employee': false,
      'Cost': false,
      'Paytype': false,
      'Contractor': false,
      'Operation': false
    },
    formOperation: '', // текущий режим открытой формы (add/edit)
    arDeleteTarget: {}, // текущий таргет для удаления, объект с моделью и id
    apiRequestInProgress: false, // статус показа прелоадера на асинхронных операциях
    // ФИЛЬТРЫ И СОРТИРОВКИ
    // поле сортировки выбранное юзером
    sortSelected: {field: 'date', order: 'desc', function: 'byDate'},
    // массив применяемых к списку Операций фильтров
    arActiveFilters: {
      paytype_id: {value: '', function: 'byFieldValue'},
      date: {value: '', function: 'byValue'},
      firm_id: {value: '', function: 'byFieldValue'},
      summ: {value: '', function: 'byNumber'},
      contractor_id: {value: '', function: 'byFieldValue'},
      employee_id: {value: '', function: 'byFieldValue'},
      manager_id: {value: '', function: 'byFieldValue'},
      account: {value: '', function: 'byValue'},
      comment: {value: '', function: 'byValue'}
    },
    // ЕЖЕМЕСЯЧНЫЕ ОПЕРАЦИИ
    MonthlyTask: DemoData.MonthlyTask
  },
  getters: {
    getEmployees: state => state.Employee,
    getCosts: state => state.Cost,
    getPaytypes: state => state.Paytype,
    getPaytypeVariants: state => state.PaytypeVariant,
    getContractors: state => state.Contractor,
    getOperations: state => state.OperationFiltered,
    getDepartments: state => state.Departments,
    nBx24ReadyCount: state => state.nBx24ReadyCount,
    isBx24Ready: state => state.isBx24Ready,
    isDemo: state => state.isDemo,
    isAppReady: state => state.isAppReady,
    arErrors: state => state.arErrors,
    arForms: state => state.arForms,
    arEditForms: state => state.arEditForms,
    formOperation: state => state.formOperation,
    arDeleteTarget: state => state.arDeleteTarget,
    apiRequestInProgress: state => state.apiRequestInProgress,
    queryMonth: state => state.queryMonth,
    queryYear: state => state.queryYear,
    modalOffset: state => state.modalOffset,
    sortSelected: state => state.sortSelected,
    MonthlyTask: state => state.MonthlyTask,
    urlPortal: state => state.urlPortal,
    getOptions: state => state.Option
  },
  actions: {
    // запускается сразу с приложением
    initBx24: (context) => {
      let BX24Script = document.createElement('script')
      BX24Script.setAttribute('src', '//api.bitrix24.com/api/v1/')
      BX24Script.onload = () => {
        console.log('BX24Script loaded')

        // прелоадер BX24
        const waitBX24Ready = () => {
          setTimeout(function () {
            try {
              context.commit('Bx24CounterInc')

              if (BX24.isReady()) {
                console.log('BX24 is ready')
                // ставим рамку iframe в нужный нам размер
                BX24.resizeWindow(document.getElementById('app').offsetWidth, 1600)
                // читаем секретный ключ
                BX24.callMethod('entity.item.get',
                  {
                    ENTITY: 'profit_b24',
                    SORT: {},
                    FILTER: {
                      'NAME': 'sSecretCode'
                    }
                  },
                  (response) => {
                    const sCode = response.answer.result[0].CODE
                    if (sCode) {
                      context.commit('setBx24Ready')
                      context.dispatch('setWorkMode', {sCode})
                    }
                  })
              }
            } catch (Error) {
              // console.log(Error)
              // else
              if (context.state.nBx24ReadyCount >= context.state.nBx24ReadyLimit) {
                // откроем окно выбора логина или демо режима
                $('#idLoginFormBtn').click()
              } else {
                // console.log('BX24 not ready...')
                waitBX24Ready()
              }
            }
          }, 500)
        }
        waitBX24Ready()
      }
      document.head.appendChild(BX24Script)
    },
    setDemoMode: (context) => {
      console.log('Fallback to DEMO mode !')
      context.commit('setDemoMode')
      // удалим первый элемент демо данных (болванка)
      context.dispatch('setDeleteTarget', {model: 'Operation', index: 0, id: 1})
      context.dispatch('actionDelete')
      // сгенерим новых
      context.dispatch('makeDemoData', {count: 10})
        .then(context.commit('setAppReadyState', {status: true}))
    },
    setWorkMode: (context, request) => {
      // пишем Код
      context.commit('setBx24SecretCode', {sCode: request.sCode})
      // ставим флаг что приложение готово
      context.commit('setAppReady')
      // грузим данные по апи
      context.dispatch('initPortalData')
    },
    // ВСЕ ОПРЕАЦИИ ПО ПЕРВОНАЧАЛЬНОЙ ПОДГРУЗКЕ ДАННЫХ
    initPortalData: (context) => {
      console.log('initPortalData')

      // грузим Отделы прямо из битрикс
      if (context.state.isBx24Ready) {
        BX24.callMethod('department.get', {}, res => {
          const arDep = res.answer.result.map(item => {
            return {'id': parseInt(item.ID), 'name': item.NAME}
          })
          context.commit('setDepartments', arDep)
        })
      } else {
        // todo когда рабоатет извне, подгрухить департаменты по апи
        context.commit('setDepartments', [])
      }

      Api.getListRequest('PaytypeVariant', context)
      Api.getListRequest('Paytype', context)
      Api.getListRequest('Contractor', context)
      Api.getListRequest('Employee', context)
      Api.getListRequest('Cost', context)
      Api.getListRequest('Option', context)

      setTimeout(function (context) {
        context.dispatch('getOperationData')
      }, 1000, context)
    },
    getOperationData: (context) => {
      console.log('[action] getOperationData = ', context.state.queryMonth + 1, context.state.queryYear)
      if (context.state.isDemo) {
        setTimeout(function () {
          context.commit('setAppReadyState', {status: true})
          context.dispatch('applyFiltersAndSort')
        }, 500)
      } else {
        // в js месяца с 0 по 11
        Api.getListRequest('MonthlyTask', context, {month: (context.state.queryMonth + 1), year: context.state.queryYear})
        Api.getListRequest('Operation', context, {month: (context.state.queryMonth + 1), year: context.state.queryYear})
      }
    },
    // Dates
    setQueryMonth: (context, request) => {
      let date = new Date(context.state.queryYear, context.state.queryMonth, 1, 0, 0, 0, 0)
      date.setMonth(date.getMonth() + parseInt(request.change))
      console.log('setQueryMonth', date.toDateString())
      context.commit('setQueryMonth', {month: date.getMonth(), year: date.getFullYear()})
    },
    // Demo data
    makeDemoData: (context, request) => {
      const demoSumm = [5000, 10000, 15000]
      const days = ['02', '03', '04', '10', '11', '12', '20', '21', '22']
      for (let i = 0; i < request.count; i++) {
        let contractor = loSampe([loSampe(context.state.Contractor.filter(it => it.is_my === 0)).id, null])
        let firm = loSampe([loSampe(context.state.Contractor.filter(it => it.is_my === 1)).id, null])
        let employee = loSampe([loSampe(context.state.Employee).id, null])
        let manager = loSampe([loSampe(context.state.Employee).id, null])

        context.dispatch('actionAdd', {
          model: 'OperationDemo',
          data: {
            'paytype_id': loSampe(context.state.Paytype).id,
            'firm_id': firm,
            'contractor_id': contractor,
            'employee_id': employee,
            'manager_id': manager,
            'summ': loSampe(demoSumm),
            'date': (new Date(context.state.queryYear, context.state.queryMonth, loSampe(days), 0, 0, 0, 0)).toISOString().slice(0, 10),
            'account': '',
            'comment': ''
          }
        })
      }
    },

    // Forms
    setFormState: (context, request) => {
      context.commit('setFormState', request)
    },
    setEditState: (context, request) => {
      context.commit('setEditState', request)
    },
    setFormOperation: (context, request) => {
      context.commit('setFormOperation', request)
    },
    setDeleteTarget: (context, request) => {
      context.commit('setDeleteTarget', request)
    },
    setFormSpinner: (context, request) => {
      context.commit('setFormSpinner', request)
    },
    setErrorState: (context, request) => {
      context.commit('setErrorState', request)
    },
    // Ready state preloader controller
    setAppReadyState: (context, request) => {
      context.commit('setAppReadyState', request)
    },
    setModalOffset: (context, request) => {
      context.commit('setModalOffset', request)
    },
    setUrlPortal: (context, request) => {
      context.commit('setUrlPortal', request)
    },

    // универсальное добавление сущности
    actionAdd: (context, request) => {
      console.log('[action] actionAdd = ', request)
      if (context.state.isDemo) {
        // в демо режиме общая модель показа и хранилища - разные
        if (request.model === 'Operation') {
          request.model = 'OperationDemo'
        }
        setTimeout(function () {
          request.data.id = (context.state[request.model].reduce((acc, item) => Math.max(acc, parseInt(item.id)), 0) + 1)
          context.commit('setErrorState', {model: request.model, 'msg': ''})
          context.commit('setFormState', {model: request.model, status: false})
          context.commit('addDataFromApi', {model: request.model, data: request.data})
          context.commit('setFormSpinner', {status: false})

          if (request.model === 'OperationDemo') {
            context.dispatch('applyFiltersAndSort')
          }
        }, 300)
      } else {
        Api.addRequest(context, request)
      }
    },
    // универсальное удаление сущности
    actionDelete: (context) => {
      const delTarget = JSON.parse(JSON.stringify(context.state.arDeleteTarget))
      console.log('[action] actionDelete = ', delTarget)
      if (context.state.isDemo) {
        // в демо режиме общая модель показа и хранилища - разные
        if (delTarget.model === 'Operation') {
          delTarget.model = 'OperationDemo'
        }
        setTimeout(function () {
          context.commit('setErrorState', {model: delTarget.model, 'msg': ''})
          context.commit('deleteDataFromApi', delTarget)
          context.commit('setDeleteTarget', {})
          context.commit('setFormSpinner', {status: false})

          if (delTarget.model === 'OperationDemo') {
            context.dispatch('applyFiltersAndSort')
          }
        }, 300)
      } else {
        Api.deleteRequest(context, delTarget)
      }
    },
    // универсальное обновление сущности
    actionUpdate: (context, request) => {
      console.log('[action] actionUpdate = ', request)
      if (context.state.isDemo) {
        // в демо режиме общая модель показа и хранилища - разные
        if (request.model === 'Operation') {
          request.model = 'OperationDemo'
        }
        setTimeout(function () {
          context.commit('setErrorState', {model: request.model, 'msg': ''})
          context.commit('setEditState', {model: request.model, index: false})
          context.commit('updateDataFromApi', request)
          context.commit('setFormSpinner', {status: false})

          if (request.model === 'OperationDemo') {
            context.dispatch('applyFiltersAndSort')
          }
        }, 300)
      } else {
        Api.updateRequest(context, request)
      }
    },
    sendTaskRequest: (context, request) => {
      console.log('[action] sendTaskRequest = ', request)
      Api.taskRequest(context, request)
    },
    // применяет к Операциям все текущие фильтры и сортирует
    applyFiltersAndSort: (context) => {
      const state = context.state
      // console.log('applyFiltersAndSort action')
      console.log('applyFiltersAndSort action', JSON.parse(JSON.stringify(state.arActiveFilters)), JSON.parse(JSON.stringify(state.sortSelected)))
      let arNewData = []
      // применяем стандартный фильтр по месяцу (вообще он нужен только для демо)
      if (state.isDemo) {
        arNewData = Filters.byCurrentMonth(state, state.OperationDemo)
      } else {
        arNewData = state.Operation
      }
      arNewData = JSON.parse(JSON.stringify(arNewData))

      // ФИЛЬТРЫ
      for (let field in state.arActiveFilters) {
        if (state.arActiveFilters.hasOwnProperty(field)) {
          const filter = state.arActiveFilters[field]
          if (filter.value) {
            console.log('Apply filter ', field, filter.value)
            arNewData = Filters[filter.function](arNewData, field, filter.value, state)
          }
        }
      }

      // СОРТИРОВКА
      const sort = JSON.parse(JSON.stringify(state.sortSelected))
      console.log('Apply sort ', sort)
      if (sort.function) {
        Sorters[sort.function](arNewData, sort.field, sort.order, state)
      } else {
        Sorters.byField(sort.field, sort.order)
      }

      context.commit('setOperationAfterFiltersAndSort', arNewData)
      context.dispatch('resizeB24Frame')
    },
    setNewSort: (context, data) => {
      context.commit('setNewSort', data)
    },
    setNewFilters: (context, data) => {
      context.commit('setNewFilters', data)
    },
    resizeB24Frame: (context) => {
      context.commit('resizeB24Frame')
    }
  },
  mutations: {
    setDemoMode: (state) => {
      state.isDemo = true // production mode
      // state.isBx24Ready = true // dev mode: для api отладки на локале
      state.isAppReady = true
    },
    // автоматический ресайз фрейма приложения
    resizeB24Frame: (state) => {
      // console.log('resizeB24Frame mutation')
      if (state.isBx24Ready) {
        setTimeout(() => {
          // console.log('Auto resize', document.querySelector('#app').offsetWidth, document.querySelector('#app').offsetHeight)
          BX24.resizeWindow(
            document.getElementById('app').offsetWidth,
            document.getElementById('app').offsetHeight + 200 // todo magic
          )
        }, 500)
      }
    },
    setOperationAfterFiltersAndSort: (state, data) => {
      // console.log('setOperationAfterFiltersAndSort mutation', JSON.parse(JSON.stringify(data)))
      console.log('setOperationAfterFiltersAndSort mutation')
      state.OperationFiltered = data
    },
    setNewSort: (state, data) => {
      state.sortSelected = data
    },
    setModalOffset: (state, data) => {
      let val = data - 200 // todo magic
      if (val < 200) {
        val = 100 // todo magic
      }
      state.modalOffset = val
    },
    setNewFilters: (state, data) => {
      console.log('setNewFilters mutation', data)
      state.arActiveFilters[data.prop].value = data.value
    },
    setBx24Ready: (state) => {
      state.isBx24Ready = true
    },
    setAppReady: (state) => {
      state.isAppReady = true
    },
    setAppReadyState: (state, data) => {
      state.isAppReady = data.status
    },
    setBx24SecretCode: (state, data) => {
      Api.setAuth(data.sCode)
    },
    setQueryMonth: (state, data) => {
      // console.log('mutating setQueryMonth', data)
      state.queryMonth = data.month
      state.queryYear = data.year
    },
    setDepartments: (state, data) => {
      // console.log('mutating setDepartments', data)
      state.Departments = data
    },
    setFormSpinner: (state, data) => {
      // console.log('mutating setFormSpinner', data.status)
      state.apiRequestInProgress = data.status
    },
    setDeleteTarget: (state, data) => {
      state.arDeleteTarget = data
    },
    setEditState: (state, data) => {
      state.arEditForms[data.model] = data.index
    },
    setFormOperation: (state, data) => {
      state.formOperation = data.value
    },
    setFormState: (state, data) => {
      state.arForms[data.model] = data.status
    },
    setErrorState: (state, response) => {
      state.arErrors[response.model] = response.msg
    },
    setUrlPortal: (state, data) => {
      state.urlPortal = data.url
    },
    // обработчик получения данных из бэкенда
    setDataFromApi: (state, response) => {
      console.log('[mutation] setDataFromApi: ', response)
      switch (response.model) {
        case 'MonthlyTask':
          state[response.model] = response.data
          break
        default:
          state[response.model] = response.data
          // хак чтобы срабатывали computed св-ва в Results компоненте
          state[response.model].push({})
          state[response.model].pop()
      }
    },
    addDataFromApi: (state, response) => {
      console.log('Async data add: ', response.data)
      state[response.model].push(response.data)
    },
    deleteDataFromApi: (state, response) => {
      console.log('Async data delete: ', response)
      state[response.model] = state[response.model].filter(item => item.id !== response.id)
    },
    updateDataFromApi: (state, response) => {
      console.log('Async data update: ', response)
      // заменить данные в таблице по ID
      let isNew = true
      state[response.model].forEach((obj, index) => {
        if (response.data.id === obj.id) {
          state[response.model][index] = response.data
          isNew = false
        }
      })

      // иногда могут прийти совершенно новые данные
      if (isNew) {
        state[response.model].push(response.data)
      }

      // хак чтобы срабатывали computed св-ва в Results компоненте
      state[response.model].push({})
      state[response.model].pop()
    },
    Bx24CounterInc: (state) => {
      state.nBx24ReadyCount++
    }
  },
  strict: process.env.NODE_ENV !== 'production'
})
