import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Bills from '../components/Bills'
import Stats from '../components/Stats'
import SetupFirms from '../components/SetupFirms'
import SetupContractors from '../components/SetupContractors'
import SetupCosts from '../components/SetupCosts'
import SetupEmployees from '../components/SetupEmployees'
import SetupPaytypes from '../components/SetupPaytypes'
import AppData from '../components/AppData'
import page404 from '../components/other/page404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/bills',
      name: 'Bills',
      component: Bills
    },
    {
      path: '/stats',
      name: 'Stats',
      component: Stats
    },
    {
      path: '/setup/firms',
      name: 'SetupFirms',
      component: SetupFirms
    },
    {
      path: '/setup/contractors',
      name: 'SetupContractors',
      component: SetupContractors
    },
    {
      path: '/setup/costs',
      name: 'SetupCosts',
      component: SetupCosts
    },
    {
      path: '/setup/employees',
      name: 'SetupEmployees',
      component: SetupEmployees
    },
    {
      path: '/setup/paytypes',
      name: 'SetupPaytypes',
      component: SetupPaytypes
    },
    {
      path: '/setup/appdata',
      name: 'AppData',
      component: AppData
    },
    {
      path: '*',
      name: 'page404',
      component: page404
    }
  ]
})
